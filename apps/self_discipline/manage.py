# coding: utf8

"""二维码管理页面"""

import datetime
from flask_restful import Resource, Api, reqparse
from apps import app
from apps.self_discipline.db import s3db

api = Api(app)

# 兑换
def exchange(award_id):
    # 余额减少

    # 插入一条兑换记录

    # 获取奖品，更新兑换时间
    pass

# 查看兑换记录

# 维护奖品

# 维护分值鼓励项，所有分值都是1
class Score(Resource):
    def get(self):
        return {}
api.add_resource(Score, "/self_discipline/score/id")

parser = reqparse.RequestParser()
parser.add_argument('name')
parser.add_argument('image_path')
class Scores(Resource):
    def get(self):
        return {}
api.add_resource(Scores, "/self_discipline/scores")

# 维护每日计分项