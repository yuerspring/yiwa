# coding: utf8

"""系统配置"""

from apps import app
from flask import jsonify, request
from yiwa.api import render_template_with_qrcode, feedback
from yiwa.config import get_value_by_path, set_value_by_path


@app.route("/system")
def system():
    interval_time = get_value_by_path("yiwa", "listening", "interval_time", default=2)
    recording_time = get_value_by_path("yiwa", "listening", "recording_time", default=2)
    return render_template_with_qrcode("system/system.html", data=locals())

@app.route("/system/set_config", methods=["POST"])
def set_config():
    if request.method == "POST":
        key = request.form.get("key", "", type=str)
        value = request.form.get("value", "", type=int)
        result = set_value_by_path("yiwa", "listening", key, value=value)
        feedback("参数更新成功" if result else "参数更新失败")
        return jsonify({"result": result is True})