# coding: utf8

"""配置文件"""
import os
import socket
from yiwa.config import get_value_by_path

# yiwa根目录
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))


# webservice
# 参考https://stackoverflow.com/questions/166506/finding-local-ip-addresses-using-pythons-stdlib
def __get_ip():
    """获取局域网地址"""
    try:
        s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        # doesn't even have to be reachable
        s.connect(('10.255.255.255', 1))
        return s.getsockname()[0]
    except:
        return '127.0.0.1'
    finally:
        s.close()
HOST = __get_ip()
PORT = 5000

# 录音间隔时间
TIME = get_value_by_path("yiwa", "listening", "interval_time", default=2)